import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  mealPlan: DS.attr(),
  rates: DS.attr()
});
