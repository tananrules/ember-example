import DS from 'ember-data';
import ApplicationSerializer from './application';
import coerceId from "ember-data/-private/system/coerce-id";

export default ApplicationSerializer.extend(DS.EmbeddedRecordsMixin, {
  extractId(modelClass, resourceHash) {
    //primaryKey
    var id = resourceHash['rates'][0]['rateToken'];
    return coerceId(id);
  },
});
